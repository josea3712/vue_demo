import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const movies = () => {
  return [
 {"id":1,"title":"Young Visiters, The","description":"Comedy|Romance","available":false},
 {"id":2,"title":"Snatch","description":"Comedy|Crime|Thriller","available":true},
 {"id":3,"title":"Ashes and Diamonds (Popiól i diament)","description":"Drama|War","available":false},
 {"id":4,"title":"Curtis's Charm","description":"Comedy|Drama","available":false},
 {"id":5,"title":"Beethoven's 2nd","description":"Children|Comedy","available":false},
 {"id":6,"title":"Murder Over New York","description":"Comedy|Crime|Mystery|Thriller","available":true},
 {"id":7,"title":"Forever Hardcore: The Documentary","description":"Documentary","available":false},
 {"id":8,"title":"Sabata","description":"Action|Western","available":false},
 {"id":9,"title":"Breathless","description":"Thriller","available":true},
 {"id":10,"title":"Hugo Pool","description":"Romance","available":true},
 {"id":11,"title":"Dorian Gray","description":"Drama|Horror|Sci-Fi","available":false},
 {"id":12,"title":"Midnight Crossing","description":"Mystery|Thriller","available":false},
 {"id":13,"title":"Miss Julie","description":"Drama","available":true},
 {"id":14,"title":"Wolf","description":"Drama|Horror|Romance|Thriller","available":false},
 {"id":15,"title":"Mirror, The (Zerkalo)","description":"Drama","available":false},
 {"id":16,"title":"Condemned, The","description":"Action|Adventure|Crime|Thriller","available":false},
 {"id":17,"title":"Hellraiser III: Hell on Earth","description":"Horror","available":false},
 {"id":18,"title":"Capote","description":"Crime|Drama","available":true},
 {"id":19,"title":"The Dark Horse","description":"Comedy","available":false},
 {"id":20,"title":"Last Seven, The","description":"Horror|Mystery|Thriller","available":true},
 {"id":21,"title":"All Quiet on the Western Front","description":"Action|Drama|War","available":true},
 {"id":22,"title":"Downloading Nancy","description":"Drama|Thriller","available":true},
 {"id":23,"title":"Flirting","description":"Drama","available":true},
 {"id":24,"title":"First Man Into Space","description":"Drama|Horror|Sci-Fi","available":true},
 {"id":25,"title":"Hanging Tree, The","description":"Drama|Romance|Western","available":true},
 {"id":26,"title":"Time of the Gypsies (Dom za vesanje)","description":"Comedy|Crime|Drama|Fantasy","available":true},
 {"id":27,"title":"Street Fight","description":"Documentary","available":true},
 {"id":28,"title":"Music in the Air","description":"Comedy|Musical|Romance","available":false},
 {"id":29,"title":"Tale of Zatoichi Continues, The (Zoku Zatôichi monogatari) (Zatôichi 2)","description":"Action|Drama","available":true},
 {"id":30,"title":"Number Two (Numéro deux)","description":"Drama","available":true},
 {"id":31,"title":"Jerichow","description":"Drama","available":true},
 {"id":32,"title":"Little Monsters","description":"Comedy","available":false},
 {"id":33,"title":"Saving Silverman (Evil Woman)","description":"Comedy|Romance","available":true},
 {"id":34,"title":"Monsieur Vincent","description":"Drama","available":false},
 {"id":35,"title":"Unforgotten: Twenty-Five Years After Willowbrook","description":"Documentary","available":false},
 {"id":36,"title":"Terror is a Man","description":"Horror|Sci-Fi","available":false},
 {"id":37,"title":"For Love of the Game","description":"Comedy|Drama","available":true},
 {"id":38,"title":"Lock, Stock & Two Smoking Barrels","description":"Comedy|Crime|Thriller","available":true},
 {"id":39,"title":"Stand Up and Fight","description":"Drama|Romance|Western","available":true},
 {"id":40,"title":"Borderland","description":"Crime|Horror|Thriller","available":true},
 {"id":41,"title":"Chop Shop","description":"Drama","available":false},
 {"id":42,"title":"Friday the 13th Part VII: The New Blood","description":"Horror","available":false},
 {"id":43,"title":"Play it to the Bone","description":"Comedy|Drama","available":false},
 {"id":44,"title":"Pickle, The","description":"Comedy","available":true},
 {"id":45,"title":"Mifune's Last Song (Mifunes sidste sang)","description":"Comedy|Drama|Romance","available":true},
 {"id":46,"title":"Gods of the Plague (Götter der Pest)","description":"Crime|Drama","available":true},
 {"id":47,"title":"Servant, The","description":"Drama","available":true},
 {"id":48,"title":"War and Peace (Jang Aur Aman)","description":"Documentary|War","available":false},
 {"id":49,"title":"Saved!","description":"Comedy|Drama","available":true},
 {"id":50,"title":"Nighthawks","description":"Action|Drama","available":true},
 {"id":51,"title":"Son of the Mask","description":"Adventure|Children|Comedy|Fantasy","available":false},
 {"id":52,"title":"Pretty/Handsome","description":"Drama","available":false},
 {"id":53,"title":"What Did the Lady Forget? (Shukujo wa nani o wasureta ka) ","description":"Comedy|Drama","available":true},
 {"id":54,"title":"Give My Regards to Broad Street","description":"Drama|Musical","available":false},
 {"id":55,"title":"Love at First Bite","description":"Comedy|Horror|Romance","available":false},
 {"id":56,"title":"Chasing Mavericks","description":"Drama","available":false},
 {"id":57,"title":"Exit Humanity","description":"Drama|Horror","available":true},
 {"id":58,"title":"Eva","description":"Drama|Fantasy|Sci-Fi","available":true},
 {"id":59,"title":"Navy Seals","description":"Action|Adventure|War","available":false},
 {"id":60,"title":"Brooklyn Castle","description":"Documentary","available":true},
 {"id":61,"title":"Good Heart, The","description":"Drama","available":true},
 {"id":62,"title":"Delicate Balance, A","description":"Drama","available":false},
 {"id":63,"title":"Man Behind the Gun, The","description":"Western","available":false},
 {"id":64,"title":"Love in Bloom","description":"Romance","available":false},
 {"id":65,"title":"Trapped","description":"Action|Thriller","available":false},
 {"id":66,"title":"Shanghai","description":"Drama|Mystery|Romance","available":true},
 {"id":67,"title":"Crazy Class Wakes Up, The (Hababam sinifi uyaniyor)","description":"Comedy","available":false},
 {"id":68,"title":"Zazie dans le métro","description":"Comedy","available":false},
 {"id":69,"title":"Good Time Max","description":"Drama","available":false},
 {"id":70,"title":"Paranoia Agent","description":"Animation|Crime|Drama|Horror|Mystery","available":false},
 {"id":71,"title":"Blind Sunflowers, The (Los girasoles ciegos)","description":"Drama","available":false},
 {"id":72,"title":"T.N.T.","description":"Action","available":false},
 {"id":73,"title":"Mating of Millie, The","description":"Comedy|Romance","available":true},
 {"id":74,"title":"A Rumor Of War","description":"(no genres listed)","available":false},
 {"id":75,"title":"Change of Address (Changement d'adresse)","description":"Comedy","available":false},
 {"id":76,"title":"Wuthering Heights","description":"Drama|Romance","available":false},
 {"id":77,"title":"Miss Julie","description":"Drama","available":false},
 {"id":78,"title":"Evilenko","description":"Crime|Horror|Thriller","available":true},
 {"id":79,"title":"Family Weekend","description":"Comedy|Drama","available":true},
 {"id":80,"title":"Our Daily Bread (Unser täglich Brot)","description":"Documentary","available":true},
 {"id":81,"title":"Guard Post, The (G.P. 506)","description":"Horror","available":false},
 {"id":82,"title":"Hatchet III","description":"Comedy|Horror","available":false},
 {"id":83,"title":"Place in the Sun, A","description":"Drama|Romance","available":true},
 {"id":84,"title":"There Goes My Baby","description":"Comedy|Drama","available":true},
 {"id":85,"title":"Xala","description":"Comedy","available":false},
 {"id":86,"title":"Ace of Hearts","description":"Children|Drama","available":false},
 {"id":87,"title":"Palms (Ladoni)","description":"Documentary","available":true},
 {"id":88,"title":"Dhoondte Reh Jaoge","description":"Comedy|Drama","available":false},
 {"id":89,"title":"Dexter the Dragon & Bumble the Bear (a.k.a. Dragon That Wasn't (Or Was He?), The) (Als je begrijpt wat ik bedoel)","description":"Animation|Children|Fantasy","available":false},
 {"id":90,"title":"Moulin Rouge","description":"Drama|Musical|Romance","available":true},
 {"id":91,"title":"Bonfire of the Vanities","description":"Comedy|Crime|Drama","available":true},
 {"id":92,"title":"Big Hangover, The","description":"Comedy","available":false},
 {"id":93,"title":"Play Motel","description":"Crime|Horror|Thriller","available":true},
 {"id":94,"title":"Philomena","description":"Comedy|Drama","available":true},
 {"id":95,"title":"Legend of Suram Fortress, The (Ambavi Suramis tsikhitsa) ","description":"Drama","available":false},
 {"id":96,"title":"Save Me","description":"Drama","available":false},
 {"id":97,"title":"White Noise 2: The Light","description":"Drama|Horror|Thriller","available":false},
 {"id":98,"title":"Little Man Tate","description":"Drama","available":false},
 {"id":99,"title":"Not on the Lips (Pas sur la bouche)","description":"Comedy|Musical|Romance","available":true},
 {"id":100,"title":"Tarda estate","description":"Drama","available":false}]
 }

export default new Vuex.Store({
  state: {
    movies: movies(),
    filter: {
      query: '',
      available: true
    }
  },
  mutations: {
    //state de vuex se envia automaticamente
    //query que ingresa por parametro, cadena que vamos a buscar
    SET_QUERY (state, query){
      state.filter.query = query;
    },
    SET_AVAILABLE (state){
      state.filter.available = ! state.filter.available;
    }
  },
  actions: {
    ////Consumo de API o base de datos
  },
  getters:{
    filteredMovies (state) {
      //se recuperan todas las películas con estado available igual al que elegimos
      let movies = state.movies.filter(movie => movie.available === state.filter.available);
      //realizamos busqueda con el query cuando la cadena es mayor que dos letras
      if(state.filter.query.length > 2){
        //retornamos todas las peliculas que hagan "match" con el query a buscar
        return state.movies.filter(movie => movie.title.toLowerCase().includes(state.filter.query));
      }
      return movies;
    }
  }
})
